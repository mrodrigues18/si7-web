<?php

class Fonction {

    private $db;
    private $insert;
    private $select;
//    private $selectCount; en commentaire plus bas 
    
    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert  into  fonction(titre) values( :titre)");                  
        $this->select = $db->prepare("select * from fonction");
        //$this->selectCount = $db->prepare("select COUNT(idFonction) AS nbF from fonction");
    }

    
    public function insert($titre) { 
        $r = true;
        $this->insert->execute(array(':titre'=>$titre));
        if ($this->insert->errorCode()!=0){
            print_r($this->insert->errorInfo());
            $r=false;
        }
        return $r;
    }
    
    
    public function select() {
        $listeF = $this->select->execute();
        if ($this->select->errorCode()!=0){
            print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
    
//    public function selectCount(){
//        $this->selectCount->execute();
//        if($this->selectCount->errorCode()!=0){
//            print_r($this->selectCount->errorInfo());
//        }
//        return $this->selectCount->fetch();
//    }
}