<?php

class Ordinateur {

    private $db;
    private $insert;
    private $select;
    private $selectById;
    private $selectCount;
    private $selectDistinctReseau;
    private $update;
    private $updatePcOnOff;
    private $selectCarousel;
    private $delete;
    private $clearScan;

    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("INSERT INTO ordinateur(ip, mac, reseau, os, statut, employe) VALUES(:ip, :mac, :reseau, :os, :statut, :employe)");
        $this->select = $db->prepare("SELECT idOrdinateur, ip, mac, reseau, nomOs, nomStatut, CONCAT(nomEmploye, ' ', prenomEmploye) AS unEmploye "
                . "FROM ordinateur o "
                . "INNER JOIN statut s ON s.idStatut = o.statut "
                . "LEFT JOIN employe e ON e.idEmploye = o.employe "
                . "INNER JOIN os ON os.idOs = o.os "
                . "ORDER BY idOrdinateur ");
        $this->selectById = $db->prepare("SELECT idOrdinateur, ip, mac, nomOs, nomStatut, CONCAT(nomEmploye, ' ', prenomEmploye) AS unEmploye FROM ordinateur o "
                . "INNER JOIN statut s ON s.idStatut = o.statut "
                . "INNER JOIN employe e ON e.idEmploye = o.employe "
                . "INNER JOIN os ON os.idOs = o.os "
                . "WHERE idOrdinateur=:idOrdinateur");
        $this->selectCount = $db->prepare("SELECT COUNT(idOrdinateur) AS nb FROM ordinateur");
        $this->selectDistinctReseau = $db->prepare("SELECT DISTINCT reseau "
                . "FROM ordinateur ");
        $this->selectCarousel = $db->prepare("SELECT idOrdinateur, ip, mac, reseau, nomOs, nomStatut, CONCAT(nomEmploye, ' ', prenomEmploye) AS unEmploye "
                . "FROM ordinateur o "
                . "INNER JOIN statut s ON s.idStatut = o.statut "
                . "LEFT JOIN employe e ON e.idEmploye = o.employe "
                . "INNER JOIN os ON os.idOs = o.os "
                . "WHERE ip LIKE CONCAT('10.239.', :nb, '.%') "
                . "ORDER BY idOrdinateur ");
        $this->update = $db->prepare("update ordinateur set ip=:ip, mac=:mac, reseau=:reseau, os=:os, statut=:statut, employe=:employe where idOrdinateur=:idOrdinateur");
        $this->updatePcOnOff = $db->prepare("UPDATE ordinateur SET statut=:statut WHERE reseau=:reseau");
        $this->delete = $db->prepare("delete from ordinateur where idOrdinateur=:idOrdinateur");
        $this->clearScan = $db->prepare("DELETE FROM ordinateur");
    }

    public function insert($ip, $mac, $reseau, $os, $status, $employe) {
        $r = true;
        echo $ip . " ";
        $t = $this->insert->execute(array(':ip' => $ip, ':mac' => $mac, ':reseau' => $reseau, ':os' => $os, ':statut' => $status, ':employe' => $employe));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function select() {
        $liste = $this->select->execute();
        if ($this->select->errorCode() != 0) {
            //print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

    public function selectById($idOrdinateur) {
        $this->selectById->execute(array(':idOrdinateur' => $idOrdinateur));
        if ($this->selectById->errorCode() != 0) {
            //print_r($this->selectById->errorInfo());
        }
        return $this->selectById->fetch();
    }

    public function selectCount() {
        $this->selectCount->execute();
        if ($this->selectCount->errorCode() != 0) {
            //print_r($this->selectCount->errorInfo());
        }
        return $this->selectCount->fetch();
    }

    public function selectDistinctReseau() {
        $this->selectDistinctReseau->execute();
        if ($this->selectDistinctReseau->errorCode() != 0) {
            print_r($this->selectDistinctReseau->errorInfo());
        }
        return $this->selectDistinctReseau->fetchAll();
    }

    public function selectCarousel($nb) {
        $this->selectCarousel->execute(array(':nb' => $nb));
        if ($this->selectCarousel->errorCode() != 0) {
            print_r($this->selectCarousel->errorInfo());
        }
        return $this->selectCarousel->fetchAll();
    }

    public function update($ip, $mac, $reseau, $os, $statut, $employe) {
        $r = true;
        $this->update->execute(array(':ip' => $ip, ':mac' => $mac, ':reseau' => $reseau, ':os' => $os, ':statut' => $statut, ':employe' => $employe));
        if ($this->update->errorCode() != 0) {
            print_r($this->update->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function updatePcOnOff($statut, $reseau) {
        $r = true;
        $this->updatePcOnOff->execute(array(':statut' => $statut,':reseau' => $reseau));
        if ($this->updatePcOnOff->errorCode() != 0) {
            print_r($this->updatePcOnOff->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function delete($idOrdinateur) {
        $r = true;
        $this->delete->execute(array(':idOrdinateur' => $idOrdinateur));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function clearScan() {
        $r = true;
        $this->clearScan->execute();
        if ($this->clearScan->errorCode() != 0) {
            print_r($this->clearScan->errorInfo());
            $r = false;
        }
        return $r;
    }

}
