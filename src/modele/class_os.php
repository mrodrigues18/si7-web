<?php

class Os {

    private $db;
    private $insert;
    private $select;
    
    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert  into  os(idOs, nomOs) values(:idOs, :nomOs)");                  
        $this->select = $db->prepare("select * from os");
    }

    
    public function insert($idOs,$nomOs) { 
        $r = true;
        $this->insert->execute(array(':idOs'=>$idOs, ':nomOs'=>$nomOs));
        if ($this->insert->errorCode()!=0){
            print_r($this->insert->errorInfo());
            $r=false;
        }
        return $r;
    }
    
    
    public function select() {
        $listeO = $this->select->execute();
        if ($this->select->errorCode()!=0){
            print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
    
}
