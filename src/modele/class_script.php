<?php

class Script {

    private $db;
    private $insert;
    private $update;
    private $delete;
    private $select;

//    private $selectCount; en commentaire plus bas 

    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert into script(nomScript, version, descScript, idOs, fichierScript) values(:nomScript, :version, :descScript, :idOs, :fichierScript)");
        $this->update = $db->prepare("update script set nomScript=:nomScript, version=:version, descScript=:descScript, idOs=:idOs where idScript=:idScript");
        $this->delete = $db->prepare("delete from script where idScript=:idScript");
        $this->select = $db->prepare("select * from script "
                . "INNER JOIN os ON os.idOs=script.idOs");
        //$this->selectCount = $db->prepare("select COUNT(idScript) AS nbS from script");
    }

    public function insert($nomScript, $version, $descScript, $idOs, $fichierScript) {
        $r = true;
        $this->insert->execute(array(':nomScript' => $nomScript, ':version' => $version, ':descScript' => $descScript, ':idOs' => $idOs, ':fichierScript' => $fichierScript));
        if ($this->insert->errorCode() != 0) {
            print_r($this->insert->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function update($nomScript, $version, $descScript, $idOs, $idScript) {
        $r = true;
        $this->update->execute(array(':nomScript' => $nomScript, ':version' => $version, ':descScript' => $descScript, ':idOs' => $idOs, ':idScript' => $idScript));
        if ($this->update->errorCode() != 0) {
            print_r($this->update->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function delete($idScript) {
        $r = true;
        $this->delete->execute(array(':idScript' => $idScript));
        if ($this->delete->errorCode() != 0) {
            print_r($this->delete->errorInfo());
            $r = false;
        }
        return $r;
    }

    public function select() {
        $listeS = $this->select->execute();
        if ($this->select->errorCode() != 0) {
            print_r($this->select->errorInfo());
        }
        return $this->select->fetchAll();
    }

//    public function selectCount(){
//        $this->selectCount->execute();
//        if($this->selectCount->errorCode()!=0){
//            print_r($this->selectCount->errorInfo());
//        }
//        return $this->selectCount->fetch();
//    }
}
