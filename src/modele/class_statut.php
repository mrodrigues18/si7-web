<?php

class Statut {

    private $db;
    private $insert;
    private $select;
//    private $selectCount; en commentaire plus bas 
    
    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert  into  statut(nomStatut) values(:nomStatut)");                  
        $this->select = $db->prepare("select * from statut");
        //$this->selectCount = $db->prepare("select COUNT(idStatut) AS nbS from statut");
    }

    
    public function insert($nomStatut) { 
        $r = true;
        $this->insert->execute(array(':nomStatut'=>$nomStatut));
        if ($this->insert->errorCode()!=0){
            print_r($this->insert->errorInfo());
            $r=false;
        }
        return $r;
    }
    
    
    public function select() {
        $listeS = $this->select->execute();
        if ($this->select->errorCode()!=0){
            print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
    
//    public function selectCount(){
//        $this->selectCount->execute();
//        if($this->selectCount->errorCode()!=0){
//            print_r($this->selectCount->errorInfo());
//        }
//        return $this->selectCount->fetch();
//    }
}