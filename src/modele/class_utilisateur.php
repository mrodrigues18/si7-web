<?php

class Utilisateur {

    private $db;
    private $insert;
    private $select;
//    private $selectCount; en commentaire plus bas 
    
    public function __construct($db) {
        $this->db = $db;
        $this->insert = $db->prepare("insert into utilisateur(id, pseudo, fonction, mdpUtilisateur) values(:id, :pseudo, :fonction,:mdpUtilisateur)");                  
        $this->select = $db->prepare("select * from utilisateur u INNER JOIN fonction f ON f.idFonction=u.fonction");
        //$this->selectCount = $db->prepare("select COUNT(id) AS nbU from utilisateur");
    }

    
    public function insert($id,$pseudo,$fonction,$mdpUtilisateur) { 
        $r = true;
        $this->insert->execute(array(':id'=>$id, ':pseudo'=>$pseudo, ':fonction'=>$fonction, ':mdpUtilisateur'=>$mdpUtilisateur));
        if ($this->insert->errorCode()!=0){
            print_r($this->insert->errorInfo());
            $r=false;
        }
        return $r;
    }
    
    
    public function select() {
        $listeU = $this->select->execute();
        if ($this->select->errorCode()!=0){
            print_r($this->select->errorInfo());  
        }
        return $this->select->fetchAll();
    }
    
//    public function selectCount(){
//        $this->selectCount->execute();
//        if($this->selectCount->errorCode()!=0){
//            print_r($this->selectCount->errorInfo());
//        }
//        return $this->selectCount->fetch();
//    }
}